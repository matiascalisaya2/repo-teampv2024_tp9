package ar.edu.unju.fi.TP9_TeamPV.service.imp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.unju.fi.TP9_TeamPV.model.Producto;
import ar.edu.unju.fi.TP9_TeamPV.repository.IProductoRepository;
import ar.edu.unju.fi.TP9_TeamPV.service.IProductoService;

@Service
public class ServiceProductoImp implements IProductoService {
	List<Producto> listaProtuctos = new ArrayList<>();
	
	@Autowired
	private IProductoRepository IproductoRepository;

	@Override
	public void agregarProducto(Producto producto) {
		/*Producto produ = buscarproducto(producto.getCodigo());
		if (produ == null) {
			listaProtuctos.add(producto);
		} else {
			int pos = listaProtuctos.indexOf(produ);
			listaProtuctos.set(pos, producto);
		}*/
		IproductoRepository.save(producto);
	}

	@Override
	public List<Producto> listaProductos() {
		//return listaProtuctos;
		return IproductoRepository.findAll();
	}

	/*@Override
	public Producto ultimoproducto() {
		Producto ultimoProducto = listaProtuctos.get(listaProtuctos.size() - 1);
		return ultimoProducto;
	}*/

	@Override
	public Producto buscarproducto(int codigo) {
	/*	Producto productoBuscado = null;
		for (Producto produc : listaProtuctos) {
			if (produc.getCodigo() == codigo) {
				productoBuscado = produc;
				break;
			}
		}
		return productoBuscado;*/
		return IproductoRepository.findById(codigo).orElse(null);
	}

	@Override
	public void eliminarproducto(Producto prod) {
		//listaProtuctos.remove(prod);
		IproductoRepository.delete(prod);
	}

	// METODOS AÑADIDOS PARA EL TP7

	@Override
	public String mostrarNombreProducto(int codigo) {
	/*	Producto productoBuscado = null;
		for (Producto produc : listaProtuctos) {
			if (produc.getCodigo() == codigo) {
				productoBuscado = produc;
				break;
			}
		}
		return productoBuscado.getNombre();*/
		Producto producto = buscarproducto(codigo);
		return (producto != null)? producto.getNombre() : null;
	}

	@Override
	public String mostrarPrecioProducto(int codigo) {
	/*	Producto productoBuscado = null;
		for (Producto produc : listaProtuctos) {
			if (produc.getCodigo() == codigo) {
				productoBuscado = produc;
				break;
			}
		}
		return " "+productoBuscado.getPrecio();*/
		Producto producto = buscarproducto(codigo);
        return (producto != null) ? String.valueOf(producto.getPrecio()) : null;
	}

	@Override
	public String mostrarMarcaProducto(int codigo) {
	/*	Producto productoBuscado = null;
		for (Producto produc : listaProtuctos) {			if (produc.getCodigo() == codigo) {
				productoBuscado = produc;
				break;
			}
		}
		return productoBuscado.getMarca();*/
		Producto producto = buscarproducto(codigo);
        return (producto != null) ? producto.getMarca() : null;
	}

}
