package ar.edu.unju.fi.TP9_TeamPV.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import ar.edu.unju.fi.TP9_TeamPV.model.Cliente;
import ar.edu.unju.fi.TP9_TeamPV.service.IClienteService;




@Controller
public class ClienteController {
	
	@Autowired
	IClienteService serviseCliente ;
	
	@GetMapping("/cliente/nuevo")
	public String getNewCliente(Model model) {
		Cliente cliente = new Cliente();
		model.addAttribute("nuevoCliente", cliente);
		return "nuevocliente";
	}
	
	@PostMapping("cliente/guardar")
	public String guardar(Cliente nuevoCliente) {
		int edad=serviseCliente.calcularEdad(nuevoCliente.getFechaNacimiento());
		nuevoCliente.setEdad(edad);
		
		String tiempoTranscurrio= serviseCliente.calcularTiempoDesdeUltimaCompra(nuevoCliente.getFechaUltimaCompra());
		nuevoCliente.setCalular1(tiempoTranscurrio);
		
		String tiempoDesdeFechaNac= serviseCliente.calcularTiempoDesdeFechaNacimiento(nuevoCliente.getFechaNacimiento());
		nuevoCliente.setCalular2(tiempoDesdeFechaNac);
		
		String tiempoRestanteParaCumpleños= serviseCliente.calcularTiempoHastaProximoCumpleaños(nuevoCliente.getFechaNacimiento());
		nuevoCliente.setCalular3(tiempoRestanteParaCumpleños);
		serviseCliente.agregarCliente(nuevoCliente);

		return "redirect:/cliente/listado";
	}
	
	@GetMapping("/cliente/listado")
	public String getMethodName(Model model) {
		List<Cliente> clientes = serviseCliente.listarClientes(); 
		model.addAttribute("clientes", clientes);
		model.addAttribute("contCliente" , serviseCliente.listarClientes().size());
		return "clientes";
	}
	
	
	
	//********* EDITAR ******************
	
	@GetMapping("/editar/{nroDocumento}")
	public String editarCliente(@PathVariable("nroDocumento") int nroDoc, Model modelo) {
		 
		 Cliente clienEditar = serviseCliente.buscarCliente(nroDoc);
		 
		 modelo.addAttribute("nuevoCliente",clienEditar);
		 
		 return "nuevocliente";
	 }
	
	
	//********* ELIMINAR ******************
	@GetMapping("/delet/{nroDocumento}")
	public String eliminarCliente(@PathVariable("nroDocumento") int nroDoc, Model modelo) {
		 
		Cliente clieEliminar = serviseCliente.buscarCliente(nroDoc);
		 serviseCliente.eliminarCliente(clieEliminar);
		
		 
		 return "redirect:/cliente/listado";
	}
	
	
	
	
}
