package ar.edu.unju.fi.TP9_TeamPV.service;

import java.util.List;
import java.util.Optional;

import ar.edu.unju.fi.TP9_TeamPV.model.Cuenta;

public interface ICuentaService {
	
	public List<Cuenta> listarCuenta();
	
	public void agregarCuenta(Cuenta cuenta);
	
	public void eliminarCuenta(int id);
	
	public Optional<Cuenta> buscraCuenta(int id);
	
	public boolean realizarCompra(double monto);
	
}
