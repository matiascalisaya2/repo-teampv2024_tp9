package ar.edu.unju.fi.TP9_TeamPV;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tp9TeamPvApplication {

	public static void main(String[] args) {
		SpringApplication.run(Tp9TeamPvApplication.class, args);
	}

}
