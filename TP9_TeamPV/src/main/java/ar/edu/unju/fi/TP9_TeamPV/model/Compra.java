package ar.edu.unju.fi.TP9_TeamPV.model;



import jakarta.persistence.Column;
import jakarta.persistence.Entity;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
@Entity
@Table(name = "COMPRAS")
public class Compra {
	
	@Id 
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	

	//indica que muchas compras pueden referirse al mismo producto.
	@ManyToOne
	@JoinColumn(name = "Id_Producto")
	@NotNull(message = "El producto no puede ser nulo")
	private Producto producto;
	
	@Column(name = "cantidad")
	@NotNull(message = "La cantidad no puede ser nula")
    @Min(value = 1, message = "La cantidad debe ser al menos 1")
	private int cantidad;
	//Contructores 
	public Compra() {
		
	}
	
	public Compra(Producto producto, int cantidad) {
		
		this.producto = producto;
		this.cantidad = cantidad;
	}

	
	//GETERS Y SETER
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Producto getProducto() {
		return producto;
	}
	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
	
	
	
	
}
