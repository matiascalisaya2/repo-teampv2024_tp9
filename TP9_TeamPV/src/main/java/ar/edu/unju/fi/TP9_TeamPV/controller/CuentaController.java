package ar.edu.unju.fi.TP9_TeamPV.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import ar.edu.unju.fi.TP9_TeamPV.model.Cuenta;
import ar.edu.unju.fi.TP9_TeamPV.service.ICuentaService;

@Controller
public class CuentaController {
	@Autowired
	ICuentaService serviceCuenta;

	@GetMapping("/cuenta/nuevo")
	public String nuevo(Model model) {
		Cuenta cuenta = new Cuenta ();
		model.addAttribute("cuenta", cuenta);
		model.addAttribute("titulo", "Agregar Cuenta");
		return "nuevoCuenta";
	}
	
	
	@PostMapping("/cuenta/guardar")
	public String guardar(Cuenta cuenta) {
		serviceCuenta.agregarCuenta(cuenta);
		
		return "redirect:/cuenta/listado";
	}
	
	@GetMapping("/cuenta/listado")
	public String listado(Model model) {
		List<Cuenta> cuentas= serviceCuenta.listarCuenta();
		model.addAttribute("cuentas", cuentas);
		model.addAttribute("contCuenta", serviceCuenta.listarCuenta().size());
		return "cuentas";
	}
	
	@GetMapping("/cuenta/eliminar/{id}")
	public String eliminar(@PathVariable("id") int id) {
		serviceCuenta.eliminarCuenta(id);
		return "redirect:/cuenta/listado";
	}
	
	@GetMapping("/cuenta/editar/{id}")
	public String editarEmpleado(@PathVariable("id") int id, Model modelo) {
		 
		Optional<Cuenta> cuentaEditar = serviceCuenta.buscraCuenta(id);
		 
		modelo.addAttribute("cuenta", cuentaEditar);
		modelo.addAttribute("titulo","Editar Cuenta");
		 return "nuevoCuenta";
	 }
	
}
