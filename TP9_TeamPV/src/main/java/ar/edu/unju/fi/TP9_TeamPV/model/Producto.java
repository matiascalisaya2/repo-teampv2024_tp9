package ar.edu.unju.fi.TP9_TeamPV.model;

import java.util.List;

import org.springframework.stereotype.Component;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

@Entity
@Table (name= "PRODUCTOS")
public class Producto {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int codigo;
	
	@NotEmpty(message = "El nombre no puede ser vacío")
    @Size(max = 20, message = "El nombre debe tener como maximo 20 caracteres")
	@Column(name="Nombre")
	private String nombre;
	
	@Min(value = 0, message = "El precio no puede ser negativo")
	@Column(name="Precio")
	private double precio;
	
	@NotEmpty(message = "Debe llenar la marca. No puede ser vacía")
	@Column (name="Marca")
	private String marca;
	
	@Min(value = 0, message = "El stock no puede ser negativo")
	@Column(name="Stock")
	private int stock;
	
	//indica que un producto puede estar en muchas compras.El mappedBy = "producto" indica que esta relación está mapeada por el atributo producto en la clase Compra.
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "producto" ) //una federacion tiene muchos clubes....   mappedBy = "federacion"  federacion es el nokmbre del onjeto k esta en la ptra clase 
	private List<Compra> compras;	
	

	//CCONSTRUCTORES
	public Producto() {}
	
	public Producto( String nombre, double precio, String marca, int stock) {
		this.nombre = nombre;
		this.precio = precio;
		this.marca = marca;
		this.stock = stock;
	}

	//GEETERS Y SETERS
	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}
	
	
	
	
}
