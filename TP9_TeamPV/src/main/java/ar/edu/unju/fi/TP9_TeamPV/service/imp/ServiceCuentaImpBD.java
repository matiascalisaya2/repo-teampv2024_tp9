package ar.edu.unju.fi.TP9_TeamPV.service.imp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.unju.fi.TP9_TeamPV.model.Cuenta;
import ar.edu.unju.fi.TP9_TeamPV.repository.ICuentaRepository;
import ar.edu.unju.fi.TP9_TeamPV.service.ICuentaService;

@Service
public class ServiceCuentaImpBD implements ICuentaService {
	
	@Autowired
	private ICuentaRepository cuentaRepository;
	
	
	@Override
	public List<Cuenta> listarCuenta() {
		
		return cuentaRepository.findAll(); //RECUPERA LA LISTA DE LBROS DE LA BD;
	}

	@Override
	public void agregarCuenta(Cuenta cuenta) {
		Optional<Cuenta> cuentaExistente = cuentaRepository.findById(cuenta.getId());
	    if (cuentaExistente.isPresent()) {
	    	cuentaRepository.save(cuenta);
	    } else {
	        // Puedes lanzar una excepción si el autor no existe
	       // throw new RuntimeException("Autor no encontrado con id: " + cuenta.getId());
	    	cuentaRepository.save(cuenta);
	    }

	}

	@Override
	public void eliminarCuenta(int id) {
		cuentaRepository.deleteById(id);

	}

	@Override
	public Optional<Cuenta> buscraCuenta(int id) {
		Optional<Cuenta> cuenta = cuentaRepository.findById(id);
		return cuenta;
	}

	@Override
	public boolean realizarCompra(double monto) {
		/*
		if (saldo >= monto) {
            saldo -= monto;
            return true;
        } else {
            return false;
        }
        */
		return false;
	}

}
