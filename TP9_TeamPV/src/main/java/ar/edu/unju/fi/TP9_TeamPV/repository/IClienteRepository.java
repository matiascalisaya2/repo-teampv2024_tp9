package ar.edu.unju.fi.TP9_TeamPV.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.edu.unju.fi.TP9_TeamPV.model.Cliente;

public interface IClienteRepository extends JpaRepository<Cliente, Integer> {

	Cliente findByNroDoc(int NroDoc);

}
