package ar.edu.unju.fi.TP9_TeamPV.service;

import java.time.LocalDate;


import java.util.List;

import ar.edu.unju.fi.TP9_TeamPV.model.Cliente;

public interface IClienteService {

	public void agregarCliente(Cliente cliente);
	
	public List<Cliente> listarClientes();
	
	public int calcularEdad(LocalDate fechaNac);
	
	public  String calcularTiempoDesdeUltimaCompra(LocalDate fechaUltimaCompra); 
    
	public String calcularTiempoDesdeFechaNacimiento(LocalDate fechaNacimiento);
	
	public String calcularTiempoHastaProximoCumpleaños(LocalDate fechaNacimiento);
	
	public Cliente buscarCliente(int NroDoc);
	
	public void eliminarCliente(Cliente clie);
	
}
