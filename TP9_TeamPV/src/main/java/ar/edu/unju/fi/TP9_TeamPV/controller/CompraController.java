package ar.edu.unju.fi.TP9_TeamPV.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import ar.edu.unju.fi.TP9_TeamPV.model.Compra;
import ar.edu.unju.fi.TP9_TeamPV.model.Producto;
import ar.edu.unju.fi.TP9_TeamPV.service.ICompraService;
import ar.edu.unju.fi.TP9_TeamPV.service.IProductoService;

@Controller
public class CompraController {
	
		@Autowired
		@Qualifier("ServiceCompraImp")
		ICompraService serviceCompra;
		
		@Autowired
		IProductoService serviceProducto;
	 
	  @GetMapping("/addcompra")
	public String getNewCompra(Model model) {
		  Compra compra= new Compra();
		model.addAttribute("titulo","Agregar Compra");   
		model.addAttribute("compraNuevo", compra);
		model.addAttribute("listaproductos", serviceProducto.listaProductos());
		return "addCompra";
	}
	  
	@PostMapping("/guardarcompra")
	public String guardar(Compra compraNuevo) {
		serviceCompra.agregarCompra(compraNuevo);
		//actualiza el stock del prod 
		Producto producto = compraNuevo.getProducto();
		//verifica si el prod ya existe
		Producto prodExiste = serviceProducto.buscarproducto(producto.getCodigo());
	    if (prodExiste != null) {
	    	//actualiza el stock del producto 
	    	int nuevoStock = prodExiste.getStock() - compraNuevo.getCantidad();
	        prodExiste.setStock(nuevoStock);
	        
	     // Guardar el producto actualizado en el servicio de productos
	        serviceProducto.agregarProducto(prodExiste); 
	        
	    } else {
	        // Manejar el caso donde el producto no existe (opcional)
	        // Aquí puedes decidir qué hacer si el producto no existe, como lanzar una excepción o manejarlo de otra manera
	    }

	    
	    return "redirect:/listaCompra";
	}

	
	
	@GetMapping("/listaCompra")
	public String getMethodName(Model model) {
		List<Compra> compras = serviceCompra.listarCompras(); 
		model.addAttribute("compras", compras);
	    model.addAttribute("contCompra" , serviceCompra.listarCompras().size());
		model.addAttribute("serviceproducto", serviceProducto);
		return "crudCompra";
	}
	//********* EDITAR ******************
	@GetMapping("/editarCompra/{id}")
	public String editarCompra(@PathVariable("id") int id, Model modelo) {
		 
		Compra compraEditar = serviceCompra.buscarCompra(id);
		 modelo.addAttribute("titulo","Editar Compra");
		 modelo.addAttribute("compraNuevo",compraEditar);
		 modelo.addAttribute("listaproductos", serviceProducto.listaProductos());
		 return "addcompra";
	 }
	
	
	//********* ELIMINAR ******************
	@GetMapping("/deletCompra/{id}")
	public String eliminarCompra(@PathVariable("id") int id, Model modelo) {
		 
		Compra compraEliminar = serviceCompra.buscarCompra(id);
		 serviceCompra.eliminarCompra(compraEliminar);
		 //actualizr el stock del prod elimndo
		 Producto producto = compraEliminar.getProducto();
	        Producto prodExiste = serviceProducto.buscarproducto(producto.getCodigo());
	        //si el prodc existe actualiza stock
	        if (prodExiste != null) {
	   
	        	prodExiste.setStock(prodExiste.getStock());
	        	//guarda en prod para ver el stock nuevo
	            serviceProducto.agregarProducto(prodExiste);
	        }
	 
		 return "redirect:/listaCompra";
	}
	  
	 
	
}
