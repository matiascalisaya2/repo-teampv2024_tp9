package ar.edu.unju.fi.TP9_TeamPV.model;

import java.time.LocalDate; 
import org.springframework.format.annotation.DateTimeFormat;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Past;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

@Entity
@Table(name = "CLIENTES")

public class Cliente {
	//AATRIBUTOS
	@Column(name = "TIPODOCUMENTO")
	@NotEmpty
	@NotNull
		private String tipoDocumento;
	
	@Id
	@Column(name = "NRODOCUMENTO")
	//LO SACO POR QUE CLIENTE YA TIENE UN DNI ASIGNADO Y UNICO
	//@GeneratedValue(strategy = GenerationType.IDENTITY) 
		private int nroDocumento;
	
	
	@Column(name = "APELLIDO")
	@NotEmpty
	@Size(min = 3, max = 100, message = "El apellido debe tener entre 3 y 100 caracteres")
		private String Apellido;
	
	
	@Column(name = "NOMBRE")
	@NotEmpty
	@Size(min = 3, max = 100, message = "El nombre debe tener entre 3 y 100 caracteres")
		private String Nombre;
	
	
	@Column(name = "EMAIL")
	@Email
	@NotEmpty
		private String email;
	
	
	@Column(name = "PASSWORD")
	@NotEmpty
		private String password;
	
	
	@Column(name = "FECHA_NACIMIENTO")
		@DateTimeFormat (pattern = "yyyy-MM-dd")
	@Past
		private LocalDate fechaNacimiento;
	
		
	@Column(name = "EDAD")
	@Min(value = 1, message = "La edad minima es 1")
	@Max(value = 90, message = "LA edad maxima es de 90")
		private int edad;
	
	
	@Column(name = "CODAREATELEFENO")
	@Size(min = 2, max = 5)
		private int codigoAreaTelefono;
	
	
	@Column(name = "NROTELEFONO")
	@Pattern(regexp = "^[9,10]+$", message = "El numero de telefono debe tener 9 o 10 digitos")
		private int nroTeléfono;
	
	
	@Column(name = "FECHA_ULTIMACOMPRA")
		@DateTimeFormat (pattern = "yyyy-MM-dd")
	@NotNull
	@Past
		private LocalDate fechaUltimaCompra;
	
	
	@Column(name = "CALCULAR1")
	@NotNull
		private String calular1;
	
	
	@Column(name = "CALCULAR2")
	@NotNull
		private String calular2;
	
	
	
	@Column(name = "CALCULAR3")
	@NotNull
		private String calular3;
	
	
	@OneToOne
	@JoinColumn(name = "id_cuenta")
	private Cuenta cuenta;
	
	
	//CONSTRUCTORES
		
	public Cliente() {
		super();
	}

	public Cliente(String tipoDocumento, String apellido, String nombre, String email, String password,
			LocalDate fechaNacimiento, int codigoAreaTelefono, int nroTeléfono, LocalDate fechaUltimaCompra) {
		super();
		this.tipoDocumento = tipoDocumento;
		Apellido = apellido;
		Nombre = nombre;
		this.email = email;
		this.password = password;
		this.fechaNacimiento = fechaNacimiento;
		this.codigoAreaTelefono = codigoAreaTelefono;
		this.nroTeléfono = nroTeléfono;
		this.fechaUltimaCompra = fechaUltimaCompra;
	}

	//GETERS Y SETERS
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public int getNroDocumento() {
		return nroDocumento;
	}
	public void setNroDocumento(int nroDocumento) {
		this.nroDocumento = nroDocumento;
	}
	public String getApellido() {
		return Apellido;
	}
	public void setApellido(String apellido) {
		Apellido = apellido;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public int getCodigoAreaTelefono() {
		return codigoAreaTelefono;
	}
	public void setCodigoAreaTelefono(int codigoAreaTelefono) {
		this.codigoAreaTelefono = codigoAreaTelefono;
	}
	public int getNroTeléfono() {
		return nroTeléfono;
	}
	public void setNroTeléfono(int nroTeléfono) {
		this.nroTeléfono = nroTeléfono;
	}
	public LocalDate getFechaUltimaCompra() {
		return fechaUltimaCompra;
	}
	public void setFechaUltimaCompra(LocalDate fechaUltimaCompra) {
		this.fechaUltimaCompra = fechaUltimaCompra;
	}
	public String getCalular1() {
		return calular1;
	}
	public void setCalular1(String calular1) {
		this.calular1 = calular1;
	}
	public String getCalular2() {
		return calular2;
	}
	public void setCalular2(String calular2) {
		this.calular2 = calular2;
	}
	public String getCalular3() {
		return calular3;
	}
	public void setCalular3(String calular3) {
		this.calular3 = calular3;
	}
	
		
		
}
