package ar.edu.unju.fi.TP9_TeamPV.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import ar.edu.unju.fi.TP9_TeamPV.model.Cuenta;

@Repository
public interface ICuentaRepository extends JpaRepository<Cuenta, Integer> {

}
