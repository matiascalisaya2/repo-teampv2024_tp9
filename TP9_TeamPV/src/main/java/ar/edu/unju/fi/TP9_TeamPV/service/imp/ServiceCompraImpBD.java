package ar.edu.unju.fi.TP9_TeamPV.service.imp;

import java.util.List;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.unju.fi.TP9_TeamPV.model.Compra;
import ar.edu.unju.fi.TP9_TeamPV.repository.ICompraRepository;
import ar.edu.unju.fi.TP9_TeamPV.service.ICompraService;


 @Service("serviceCompraImpBD")
public class ServiceCompraImpBD implements ICompraService {

	

	@Autowired
	private ICompraRepository compraRepository;
	
	@Override
	public List<Compra> listarCompras() {
		
		return compraRepository.findAll();
	}

	@Override
	public void agregarCompra(Compra compra) {
		Optional<Compra> compraExistente = compraRepository.findById(compra.getId());
	    if (compraExistente.isPresent()) {
	    	compraRepository.save(compra);
	    } else {
	        // Puedes lanzar una excepción si el autor no existe
	       // throw new RuntimeException("Autor no encontrado con id: " + cuenta.getId());
	    	compraRepository.save(compra);
	    }
	}

	@Override
	public Compra buscarCompra(int id) {
		
		
		return null;
	}

	@Override
	public Optional<Compra> buscarComprabd(int id) {
		
		return Optional.empty();
	}

	@Override
	public void eliminarCompra(Compra compra) {
	
	}

	@Override
	public void elimianrCompraBD(int id) {
		compraRepository.deleteById(id);


	}

}
