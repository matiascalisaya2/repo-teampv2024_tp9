package ar.edu.unju.fi.TP9_TeamPV.service;

import java.util.List;

import ar.edu.unju.fi.TP9_TeamPV.model.Producto;



public interface IProductoService {

	public void agregarProducto(Producto producto);
	
	public List<Producto> listaProductos();
	
	//public Producto ultimoproducto();
	
	public Producto buscarproducto(int codigo);
	
	public void eliminarproducto(Producto prod);
	//METODOS AGREGADOS PARA TP7
	
		public String mostrarNombreProducto(int codigo);
		public String mostrarPrecioProducto(int codigo);
		public String mostrarMarcaProducto(int codigo);
}
