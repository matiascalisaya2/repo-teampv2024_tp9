package ar.edu.unju.fi.TP9_TeamPV.model;

import java.time.LocalDate;



import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PastOrPresent;
import jakarta.validation.constraints.PositiveOrZero;

@Entity
@Table(name = "CUENTAS")
@Component
public class Cuenta {
	//ATRIBUTOS
		@Id
		@Column(name = "Id")
		@GeneratedValue(strategy = GenerationType.IDENTITY) //
	    private int id;
		
		@Column(name = "Saldo")
		@NotNull
	    @PositiveOrZero //Asegura que el valor numérico del atributo sea mayor o igual a cero
	    private double saldo;
		
		@Column(name = "FechaCreacion")
		@NotNull //Asegura que el valor del atributo no sea null 
		@PastOrPresent //Asegura que el valor de la fecha o el tiempo del atributo sea una fecha o tiempo pasado o presente
	    @DateTimeFormat (pattern = "yyyy-MM-dd") //ME DAFORMATO AÑO,MES,DIA
	    private LocalDate fechaCreacion;
	//CONTRUCTORES

		public Cuenta() {
			
		}
		public Cuenta(@NotNull @PositiveOrZero double saldo, @NotNull @PastOrPresent LocalDate fechaCreacion) {
		
			this.saldo = saldo;
			this.fechaCreacion = fechaCreacion;
		}
	//GETERS Y SETERS	
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public double getSaldo() {
			return saldo;
		}
		public void setSaldo(double saldo) {
			this.saldo = saldo;
		}
		public LocalDate getFechaCreacion() {
			return fechaCreacion;
		}
		public void setFechaCreacion(LocalDate fechaCreacion) {
			this.fechaCreacion = fechaCreacion;
		}
		
		
}
