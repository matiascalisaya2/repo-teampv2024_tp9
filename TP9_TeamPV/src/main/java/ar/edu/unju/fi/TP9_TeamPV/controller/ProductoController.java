package ar.edu.unju.fi.TP9_TeamPV.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import ar.edu.unju.fi.TP9_TeamPV.model.Producto;
import ar.edu.unju.fi.TP9_TeamPV.service.IProductoService;
import jakarta.validation.Valid;

@Controller
public class ProductoController {
	
	@Autowired
	IProductoService serviceProducto;
	
	@GetMapping({"/" , "/index"})
	public String inicio() {
		
		return "index";
	}	
	/* //cunado hay mas de un servicio
	IProductoService serviceProducto=new ServiceProductoImp(); */	
	@GetMapping("/producto")
	public String getNewProducto(Model model) {
		Producto producto= new Producto();
		model.addAttribute("productoNuevo", producto);		
		return "nuevoProducto";
	}
	
	@PostMapping("/producto/guardar")
	//@Valid Producto productoNuevo: el objeto productoNuevo es validado automáticamente basado en las anotaciones de validacion
	//BindingResult result: este objeto contiene los resultados de la validacion si hay errores de validacion estaran en este objeto
	public String guardar(@Valid Producto productoNuevo, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("productoNuevo", productoNuevo);
            return "nuevoProducto";
        }
        serviceProducto.agregarProducto(productoNuevo);
        return "resultado";
    }
	
	@GetMapping("/producto/ultimo")
	public String getMethodName(Model model) {
		//Producto productos= serviceProducto.ultimoproducto();
	/*	List<Producto> productos= serviceProducto.listaProductos();
		model.addAttribute("productos", productos);
		model.addAttribute("contProd" , serviceProducto.listaProductos().size());
	return "listarproducto";*/
		List<Producto> productos = serviceProducto.listaProductos();
	        model.addAttribute("productos", productos);
	        model.addAttribute("contProd", productos.size());
	        return "listarproducto";
	}
	
	
	@GetMapping("/edit/{codigo}")
	public String editarEmpleado(@PathVariable("codigo") int codPro, Model modelo) {
		 
		 Producto prodEditar = serviceProducto.buscarproducto(codPro);
		 
		 modelo.addAttribute("productoNuevo",prodEditar);
		 
		 return "nuevoProducto";
	 }
	
	
	//********* ELIMINAR ******************
	@GetMapping("/del/{codigo}")
	public String eliminarEmpleado(@PathVariable("codigo") int codPro, Model modelo) {
		 
		/* Producto prodEliminar = serviceProducto.buscarproducto(codPro);
		 serviceProducto.eliminarproducto(prodEliminar);		 
		 return "redirect:/producto/ultimo";*/
		Producto prodEliminar = serviceProducto.buscarproducto(codPro);
        if (prodEliminar != null) {
            serviceProducto.eliminarproducto(prodEliminar);
        }
        return "redirect:/producto/ultimo";
	}
	
	
	
}
