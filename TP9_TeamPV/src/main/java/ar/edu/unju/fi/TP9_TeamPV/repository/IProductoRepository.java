package ar.edu.unju.fi.TP9_TeamPV.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.edu.unju.fi.TP9_TeamPV.model.Producto;

public interface IProductoRepository extends JpaRepository<Producto, Integer>{

}
