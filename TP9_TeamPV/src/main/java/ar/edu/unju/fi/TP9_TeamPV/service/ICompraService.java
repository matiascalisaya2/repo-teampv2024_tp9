package ar.edu.unju.fi.TP9_TeamPV.service;

import java.util.List;
import java.util.Optional;

import ar.edu.unju.fi.TP9_TeamPV.model.Compra;

public interface ICompraService {
	public List<Compra> listarCompras();
	public void agregarCompra(Compra compra);
	public Compra buscarCompra(int id) ;
	public Optional<Compra> buscarComprabd(int id);
	public void eliminarCompra(Compra compra);
	public void elimianrCompraBD(int id);

}
