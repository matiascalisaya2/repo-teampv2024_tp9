package ar.edu.unju.fi.TP9_TeamPV.service.imp;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ar.edu.unju.fi.TP9_TeamPV.model.Cliente;
import ar.edu.unju.fi.TP9_TeamPV.repository.IClienteRepository;
import ar.edu.unju.fi.TP9_TeamPV.service.IClienteService;
//import ar.edu.unju.fi.TP9_TeamPV.util.ListaDeClientes;
@Service
public class ServiceClienteImp implements IClienteService {
	
	
	@Autowired
	private IClienteRepository IclienteRepository;
	//aqui creee una lista y la use desde aqui
	//List<Cliente> listaClientes = new ArrayList<>();
	//aui pense que devia instanciar el objeto para poder usar la lista esde otro paquete
	//ListaDeClientes listaClientes1 = new ListaDeClientes();//instancie el objeto donde esta la lisita de clientes
	@Override
	public void agregarCliente(Cliente cliente) {
		//aqui uso el objeto y yamo al atributo que es una lista y llamo ala funcion add que me agregar un objeto ala lista
		//PUEDO USAR SU ATRIBUTO LIBREMENTE POR QUE ESTA PUBLICO
		
		/*Cliente clien= buscarCliente(cliente.getNroDocumento());
		if (clien==null) {
			ListaDeClientes.listaClientes.add(cliente); //aqui yamo ala clase donde se encuentra la variable y no nesesito metoos para acceder al atributo
		}else {
			int pos=ListaDeClientes.listaClientes.indexOf(clien);
			ListaDeClientes.listaClientes.set(pos, cliente);
		}*/
		
		IclienteRepository.save(cliente);
		
	}

	@Override
	public List<Cliente> listarClientes() {
		
		return IclienteRepository.findAll();
       //return ListaDeClientes.listaClientes;
	}

	@Override
	public int calcularEdad(LocalDate fechaNac) {
		LocalDate fechaActual = LocalDate.now();
		int edad = Period.between(fechaNac, fechaActual).getYears();
	    return (edad);
	}

	
	@Override
	public  String calcularTiempoDesdeUltimaCompra(LocalDate fechaUltimaCompra) {
        // PRIMERO OBTENGO LA FECHA ACTUAL
        LocalDate fechaActual = LocalDate.now();
        
        // ALCULAMOS LA DIFERENCIA ENTRE LAS DOS FECHAS Y LOS GUARDO EN UNA VARIALBLE DE TIPO PERIOD
        Period periodo = Period.between(fechaUltimaCompra, fechaActual);
        
        // UNA VEZ OBTENIDO LA FECHA PODEMOS EXTRAER LAS PARTES PARA CAMBIARLE EL FORMATO
        int años = periodo.getYears();  //HACI OBTENGO LOS AÑOS DE UNA VARIABLE PERIOD
        int meses = periodo.getMonths();//HACI OBTENGO LOS MESES DE UNA VARIABLE PERIOD
        int dias = periodo.getDays();   //HACI OBTENGO LOS DIAS DE UNA VARIABLE PERIOD
        
        // CONTRUIMOS EL FORMATO DESEADO
        String tiempoTranscurrido = años + "años/" + meses + "meses/" + dias +"dias";
        
        return tiempoTranscurrido;
        
       
        
    }

	@Override
	public String calcularTiempoDesdeFechaNacimiento(LocalDate fechaNacimiento) {
		 
        LocalDate fechaActual = LocalDate.now();
        
        // Calcular la diferencia en días entre las dos fechas
        long díasTranscurridos = ChronoUnit.DAYS.between(fechaNacimiento, fechaActual);
        
        return díasTranscurridos+" dias";
	}
	

	@Override
	public String calcularTiempoHastaProximoCumpleaños(LocalDate fechaNacimiento) {
		// Obtener la fecha actual
        LocalDate fechaActual = LocalDate.now();
        
        // Calcular la fecha del próximo cumpleaños en el año actual
        LocalDate proximoCumpleaños = fechaNacimiento.withYear(fechaActual.getYear());
        if (proximoCumpleaños.isBefore(fechaActual) || proximoCumpleaños.isEqual(fechaActual)) {
            proximoCumpleaños = proximoCumpleaños.plusYears(1);
        }
        
        // Calcular la diferencia entre la fecha del próximo cumpleaños y la fecha actual
        Period periodo = Period.between(fechaActual, proximoCumpleaños);
        long horasRestantes = ChronoUnit.HOURS.between(LocalDateTime.now(), proximoCumpleaños.atTime(LocalTime.MAX)) % 24;
        long minutosRestantes = ChronoUnit.MINUTES.between(LocalDateTime.now(), proximoCumpleaños.atTime(LocalTime.MAX)) % 60;
        long segundosRestantes = ChronoUnit.SECONDS.between(LocalDateTime.now(), proximoCumpleaños.atTime(LocalTime.MAX)) % 60;
        
        // Construir la cadena de texto en el formato deseado
        String tiempoRestante = periodo.getDays() + " dias/" + periodo.getMonths() + "meses /" + periodo.getYears() +
                                "años  " + horasRestantes + " hor:" + minutosRestantes + " min:" + segundosRestantes+ " seg";
        
        return tiempoRestante;
	}

	@Override
	public Cliente buscarCliente(int NroDoc) {
		
		 /*Cliente clienteBuscado=null;
		
		for (Cliente clie : ListaDeClientes.listaClientes) {
			if (clie.getNroDocumento()==NroDoc) {
				clienteBuscado=clie;
				break;
			}
		}
		return clienteBuscado; */
		return IclienteRepository.findByNroDoc(NroDoc);

	}

	@Override
	public void eliminarCliente(Cliente clie) {
		//ListaDeClientes.listaClientes.remove(clie);
		IclienteRepository.delete(clie);
		
	}

	

}
